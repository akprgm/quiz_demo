<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#4EBDE7" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title> @yield('title') </title>
        
        <!-- Stylesheets -->
        @include('web.partials.cssincludes')

    </head>
    <body class="{{$active_section}}">
        <!-- Header -->
        @include('web.partials.header')

        <!-- Page Content -->
        @yield('content')

        <!-- Footer -->
        @include('web.partials.footer')

        <!-- Scripts -->
        @include('web.partials.jsincludes')
    </body>
</html>
