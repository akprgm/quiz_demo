@extends('web.layouts.master', [
  'active_section' => $layout_data['active_section']
])

@section('title', 'Leader Board')

@section('content')

<div class="body-container container col-xs-10 col-xs-push-1 col-sm-8 col-sm-push-2">
	<table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Score</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody >
      @foreach ($users as $user)
      <tr>
        <td title="">
          <img src="{{ $user->avatar }}" alt="{{ $user->name }}" class="img-circle" width="30"/>
          {{ $user->name }}
        </td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->score }}</td>
        <td>{{ $user->updated_at}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

  {!! $users->render() !!}

</div>
@endsection