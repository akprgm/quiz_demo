@extends('web.layouts.master', [
    'active_section' => $layout_data['active_section'],
])

@section('title', 'Quiz App')

@section('content')

<div class="body-container container col-xs-8 col-xs-push-2 col-sm-4 col-sm-push-4">
  @if($errors->has('connection_id'))
    <p class="error"> {{$errors->get('email')[0]}} </p>
  @elseif($errors->has('answer_id'))
    <p> class="error"> {{$errors->get('name')[0]}} </p>
  @endif
  <div class="row">
    <div class="col-sm-12">
      <div class="card hovercard">
          <div class="cardheader">
          </div>
          <div class="avatar">
              <img alt="" src="{{$user->avatar}}">
          </div>
          <div class="info">
              <p>Welcome {{$user->name}} ;)</p>
          </div> 
      </div>
      <div class="row text-center">
          <a class="btn btn-primary btn-md" href="{{route('quiz.start')}}">
            Start Quiz
          </a>
          <a class="btn btn-primary btn-md" href="{{route('leader-board')}}">
            Show Leader Board
          </a>
      </div>
    </div>
  </div>
</div>
@endsection