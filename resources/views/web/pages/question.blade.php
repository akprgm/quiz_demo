@extends('web.layouts.master', [
	'active_section' => $layoutData['active_section']
])

@section('title', 'Questions')

@section('content')
<div class="col-xs-12 start-time-div text-center">
	<h2>Time Remaining <strong class="start-time">20</strong> Sec</h2>
</div>
<div class="body-container container col-xs-8 col-xs-push-2 col-sm-8 col-sm-push-2">
	<div class="col-xs-12">
		<form class="form-horizontal" role="form" method="POST" action="{{ route('quiz.answer') }}">
			{{ csrf_field() }}
			<h>{{$question_count+1}}. {{$question->question}}</h3>
			<input hidden name="question_id" value="{{$question->id}}">
			<input hidden name="answer_id" value="0">
			<input hidden name="end_time" value="0">
			<div class="row answer-div">
				@foreach($question->answers as $answer)
				<div class="col-xs-12">
					<label class="radio">
				      <input type="radio" value="{{$answer->id}}" name="answer_id">{{$answer->answer}}
				    </label>	
				</div>
				@endforeach	
			</div>
			<input class="btn text-center" type="submit"  value="Sumbit">
		</form>	
	</div>
</div>
@endsection