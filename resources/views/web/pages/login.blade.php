@extends('web.layouts.master', [
'active_section' => $layout_data['active_section']
])

@section('title', 'Login|SignUp')

@section('content')

<div class="body-container container col-xs-10 col-xs-push-1 col-sm-4 col-sm-push-4">
  <div class="onl_login">
    <h3 class="onl_authTitle" data-toggle="tooltip" data-placement="bottom" >Login With</h3>
    <div class="row onl_row-sm-offset-3 onl_socialButtons text-center" >
      @if($errors->has('email'))
      <p class="error"> {{$errors->get('email')[0]}} </p>
      @elseif($errors->has('name'))
      <p> class="error"> {{$errors->get('name')[0]}} </p>
      @endif
      <div class="col-xs-6 col-sm-4 col-sm-push-4">
        <a href="{{ url('/oauth/facebook') }}" class="btn btn-md btn-block onl_btn-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
          <i class="fa fa-facebook fa"></i>
        </a>
      </div> 
      <!-- <div class="col-xs-6 col-sm-3 col-sm-push-3">
        <a href="{{ url('/oauth/google') }}" class="btn btn-md btn-block onl_btn-google-plus" data-toggle="tooltip" data-placement="top" title="Google Plus">
          <i class="fa fa-google-plus"></i>
        </a>
      </div> -->
    </div>  
  </div>
</div>
@endsection