<?php

namespace App\Http\Repositories;

use Auth;
use App\Models\User;

class UserRepository
{
  /**
  * find User by Id
  *
  * @param $query_data
  * @return mixed
  */
  public function where($query_data) {
    return User::where($query_data);
  }

  /**
  * create new User
  *
  * @param $user_data
  * @return mixed
  */
  public function create($user_data) {
    return User::create($user_data);
  }

  /**
  * get all user by socre
  *
  * @return mixed
  */
  public function allOrderByScore() {
    return User::orderBy('score', 'desc');
  }

}
