<?php

namespace App\Http\Repositories;

use Log;
use App\Models\QuestionResponse;

class QuestionResponseRepository
{
  /**
  * get where
  *
  * @return mixed
  */
  public function getWhere($query_data) {
    return QuestionResponse::where($query_data);
  }

  /**
  * create new question response
  *
  * @return mixed
  */
  public function create($query_data) {
    return QuestionResponse::create($query_data);
  }

  /**
  * update question response
  *
  * @return mixed
  */
  public function update($question_response, $update_data) {
  	return $question_response->update($update_data);
  }       
}
