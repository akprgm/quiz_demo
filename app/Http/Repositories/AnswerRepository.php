<?php

namespace App\Http\Repositories;

use Log;
use App\Models\Answer;

class AnswerRepository
{
    /**
    * get where
    *
    * @return mixed
    */
    public function getWhere($query_data) {
        return Answer::where($query_data);
    }
}
