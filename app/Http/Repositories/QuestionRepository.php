<?php

namespace App\Http\Repositories;

use Log;
use App\Models\Question;

class QuestionRepository
{

  /**
  * get where field not in
  *
  * @return mixed
  */
  public function getWhereNotIn($field, $query_data) {
    return Question::whereNotIn($field, $query_data);
  }

  /**
  * get random questions
  *
  * @return mixed
  */
  public function getRandomQuestion($query_data) {
  	return $this->getWhereNotIn('id', $query_data)->inRandomOrder()->limit(1);
  }      
}
