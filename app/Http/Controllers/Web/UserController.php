<?php

namespace App\Http\Controllers\Web;

use Auth;
use Socialite;
use Validator;
use App\Utilities\FormRules;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserService;

class UserController extends Controller
{
  use FormRules;

  /**
   * Variables
   *
   * @var $user_service
   */
  protected $user_service;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(UserService $user_service)
  {
    $this->user_service = $user_service;
  }

  /**
   * return home page for logged in user
   *
   * @return \Illuminate\View\View
   */
  public function index() {
    if (auth()->check()){
      $layout_data = [
        'active_section' => 'home',
      ];
      
      $user = auth()->user();

      return view('web.pages.home', compact('layout_data', 'user'));
    }

    return redirect()->route('login');
  }

  /**
   * return login page
   *
   * @return \Illuminate\View\View
   */
  public function login() {
    if (auth()->check()){
      return view('web.pages.home');
    }
    $layout_data = [
      'active_section' => 'login',
    ];
    
    return view('web.pages.login', compact('layout_data'));
  }    

  /**
   * Redirect the user to the OAuth Provider.
   *
   * @return \Illuminate\Http\Response
   */
  public function redirectToOauth($oauth_type)
  {   
    if (auth()->check()){
      return view('web.pages.home');
    }
    return Socialite::driver($oauth_type)->redirect();
  }

  /**
   * Obtain the user information from provider.  Check if the user already exists in our
   * database by looking up their provider_id in the database.
   * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
   * redirect them to the authenticated users homepage.
   *
   * @return \Illuminate\Http\Response
   */
  public function handleOauthCallback($oauth_type)
  {   
    if (auth()->check()){
      return view('web.pages.home');
    }
    
    $user = Socialite::driver($oauth_type)->user();

    $user_validation =  Validator::make([
      'email'=>$user->email, 
      'name'=>$user->name
    ], 
    $this->userRegistrationRules()
  );

      //redirect back if validation fails with old inputs and errors
    if ($user_validation->fails()) {
      return redirect()->back()
      ->withErrors($user_validation);
    }

    $authUser = $this->user_service->findOrCreateUser($user, $oauth_type);
    
    Auth::login($authUser);
    
    if (!auth()->check()) {
      return redirect()->route('login');
    }
    
    return redirect()->route('home');
  }
}
