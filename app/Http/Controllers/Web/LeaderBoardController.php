<?php

namespace App\Http\Controllers\Web;

use Auth;
use Socialite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserService;

class LeaderBoardController extends Controller
{

  /**
   * Variables
   *
   * @var $user_service
   */
  protected $user_service;



  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(UserService $user_service)
  {
    $this->user_service = $user_service;
  }

  /**
   * return home page for logged in user
   *
   * @return \Illuminate\View\View
   */
  public function index() {
    $users = $this->user_service->allOrderByScore()->paginate();

    $layout_data = [
      'active_section' => 'leader-board',
    ];
    

    return view('web.pages.leader-board', compact('layout_data', 'users'));
  }
}
