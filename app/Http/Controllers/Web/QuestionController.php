<?php

namespace App\Http\Controllers\Web;

use Auth;
use Exception;
use Validator;
use App\Utilities\FormRules;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\QuestionService;

class QuestionController extends Controller
{
  use FormRules;
  /**
   * Variables
   *
   * @var $question_service
   */
  protected $question_service;



  /**
   * Create a new controller instance
   *
   * @return void
   */
  public function __construct(QuestionService $question_service)
  {
    $this->question_service = $question_service;
  }

  /**
   * start quiz
   *
   * @return mixed
   */
  public function index() {
    session(['quiz_status' => 1]); 

    return redirect()->route('quiz.question');
  }

  /**
   * Get question
   *
   * @return mixed
   */
  public function getQuestion() {

    if (!session('quiz_status')) {
      return redirect()->route('home');
    }

    $question_count = auth()->user()->questionAttempted()->count();

    if ($question_count >= config('quiz.max_question_to_show')) {
      return redirect()->route('quiz.end');
    }

    $question = $this->question_service->getQuestion();

    $layoutData = [
      'active_section' => 'question',
    ];

    return view('web.pages.question', compact('layoutData','question', 'question_count'));
  }

  /**
   * Answer question
   *
   * @return mixed
   */
  public function answerQuestion(Request $request) {
    $data = $request->all();
    
    $answer_validation =  Validator::make(
      $data, 
      $this->answerSubmitRules(),
      $this->answerSubmitMessages()
    );

      //redirect back if validation fails with old inputs and errors
    if ($answer_validation->fails()) {
      session(['quiz_status' => 0]);

      return redirect()->route('home')
      ->withErrors($answer_validation);
    }

    $this->question_service->answerQuestion($data);

    return redirect()->route('quiz.question');
  }

  /**
   * store time for question
   *
   * @return mixed
   */
  public function storeStartTime(Request $request){
    $data = $request->all();
    $this->question_service->storeStartTime($data);
    return response()->json(['status'=>true]);
  }

  /**
   * end quiz
   *
   * @return mixed
   */
  public function endQuiz(){
    $this->question_service->endQuiz();
    return redirect()->route('leader-board');
  }

}
