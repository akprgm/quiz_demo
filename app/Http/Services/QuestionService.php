<?php

namespace App\Http\Services;

use Mail;
use App\Mail\ScoreUpdate;
use App\Http\Repositories\UserRepository;
use App\Http\Repositories\QuestionRepository;
use App\Http\Repositories\QuestionResponseRepository;
use App\Http\Repositories\AnswerRepository;


class QuestionService {

  /**
   * Variables
   *
   * @var quiz_repository
   * @var quiz_response_repository
   * @var answer_repository
   * @var user_repository
   */
  protected $question_repository;
  protected $question_response_repository;
  protected $answer_repository;


  /**
   * QuestinService constructor.
   *
   * @param UserRepository $quiz_repository
   */
  public function __construct(
    QuestionRepository $question_repository, 
    QuestionResponseRepository $question_response_repository,
    AnswerRepository $answer_repository,
    UserRepository $user_repository
  )
  {
    $this->question_repository = $question_repository;
    $this->question_response_repository = $question_response_repository;
    $this->answer_repository = $answer_repository;
    $this->user_repository = $user_repository;
  }

  /**
   * get quiz
   *
   * @return mixed
   */
  public function getQuestion() {
    
    $question_already_served = auth()->user()->questionAttempted();

    $question_already_served_ids = $question_already_served->pluck('id')->toArray();

    $question = $this->question_repository->getRandomQuestion($question_already_served_ids)->with('answers')->first();

    $this->loadQuestion($question);


    return $question;
  }

  /**
   * answer quiz
   *
   * @return mixed
   */
  public function answerQuestion($query_data) {
    $question_response = $this->question_response_repository
      ->getWhere([
        ['user_id', auth()->user()->id],
        ['question_id', $query_data['question_id']]
      ])->get()->first();

    if ($query_data['end_time'] - $question_response->start_time >20) {
      $query_data['answer_id'] = 0;
    }

    
    return $this->question_response_repository->update($question_response, ['answer_id'=>$query_data['answer_id']]);

  }

  /**
   * end quiz
   *
   * @return mixed
   */
  public function endQuiz() {

    $question_already_served = auth()->user()->questionAttempted()->get();

    return $this->calculateScore($question_already_served);

  }

  /**
   * store time for question
   *
   * @return mixed
   */
  public function storeStartTime($query_data){
   $question_response = $this->question_response_repository
    ->getWhere([
      ['user_id', auth()->user()->id],
      ['question_id', $query_data['question_id']]
    ]);
  
    return $this->question_response_repository->update($question_response, ['start_time'=>$query_data['start_time']]);
  }
       
  /**
   * calculate score for user
   *
   * @return mixed
   */
  private function calculateScore ($question_already_served) {

    // generate score for user
    session('quiz_status', 0);//quiz ends 

    $user_score = 0;

    $question_already_served->map(function($question) use (&$user_score){

      if (!$question->answer_id){
        $question->delete();
        return;
      }

      $answer = $this->answer_repository->getWhere([
        ['id','=',$question->answer_id]
      ])->first();

      if ($answer->is_correct) {
        $user_score+=1;
      }
      $question->delete();
      
    });

    $user = auth()->user();
    
    $user->update(['score'=>$user_score, 'attempts' => $user->attempts+1]);

    return $this->SendScoreUpdateMail();

  }

  /**
   * Load new question for user
   *
   * @return mixed
   */
  private function loadQuestion($question) {

    $question_response_data = [
      'user_id' => auth()->user()->id,
      'question_id' => $question->id
    ];

    return $this->question_response_repository->create($question_response_data);
  }

  /**
   * Send mail to all user whose score is beaten
   */
  private function SendScoreUpdateMail() {
    $auth_user = auth()->user();

    $users = $this->user_repository->where([
      ['score','<', $auth_user->score]
    ])->get();

    foreach ($users as $key => $user) {
      Mail::to($user->email)
        ->send(new ScoreUpdate($auth_user));   
    }
  }

}