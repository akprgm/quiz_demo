<?php

namespace App\Http\Services;

use App\Http\Repositories\UserRepository;

class UserService {

  /**
   * Variables
   *
   * @var UserRepository
   */
  protected $user_repository;


  /**
   * UserService constructor.
   *
   * @param UserRepository $user_respositry
   */
  public function __construct(UserRepository $user_repository) {
    $this->user_repository = $user_repository;
  }

  /**
   * find or create user
   *
   * @param $user_data
   * @return mixed
   */
  public function findOrCreateUser($user_data, $oauth_type) {

    $user = $this->user_repository->where([
        ['email', $user_data->email],
        ['oauth_id', $user_data->id]
    ])->first();

    if ($user) {
        return $user;
    }

    return $this->user_repository->create([
        'name'  =>  $user_data->name,
        'email' =>  $user_data->email,
        'avatar' => $user_data->avatar,
        'oauth_type' => $oauth_type,
        'oauth_id' => $user_data->id
      ]
    );
  }

  /**
   * get users by score
   *
   * @return mixed
   */
  public function allOrderByScore() {
    return $this->user_repository->allOrderByScore();
  }
}