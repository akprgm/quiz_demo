<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScoreUpdate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var $user
    */
    protected $user;
    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('web.emails.score-update')
        ->subject('Retry Quiz, your score is beaten')
        ->with([
          'user_name'  =>  $this->user->name,
        ]);
    }
}
