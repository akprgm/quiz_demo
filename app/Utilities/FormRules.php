<?php

namespace App\Utilities;

trait FormRules {

  /**
   * Rules used for validating new user Registration
   *
   * @return array
   */
  public function userRegistrationRules()
  {

    return [
      'email'=> 'required|email', 
      'name'=> 'required|min:2',
    ];
  }

  /**
   * Rules used for validating answer submit by user
   *
   * @return array
   */
  public function answerSubmitRules()
  {
    return [
      'question_id'=> 'required', 
      'answer_id'=> 'required',
      'end_time'=> 'required'
    ];
  }

  /**
   * Messages used for validating answer submit by user
   *
   * @return array
   */
  public function answerSubmitMessages()
  {
    return [
      'question_id.*'=> 'Unable to process request, please try later', 
      'answer_id.*'=> 'Unable to process request, please try later',
      'end_time.*'=> 'Unable to process request, please try later'
    ];
  }

}