<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class QuestionResponse extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'user_id', 'question_id', 'answer_id', 'is_correct'
  ];

  //------------------------------------------------
	//                RELATIONSHIPS
	//------------------------------------------------

	/**
	* Get all of the answer's
	*
	* @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	*/
	public function user()
	{
		return $this->belongTo('App\Models\User');
	}

}
