<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
	/**
   * The attributes that are mass assignable.
   *
   * @var array
   */
	protected $fillable = [
		'question_id', 'answer', 'is_correct'
	];

  //------------------------------------------------
	//                RELATIONSHIPS
	//------------------------------------------------

	/**
	* Get all of the answer's
	*
	* @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	*/
	public function question()
	{
		return $this->belongsTo('App\Models\Question');
	}

}
