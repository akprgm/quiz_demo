<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'name', 'email', 'avatar', 'oauth_type', 'oauth_id', 'score'
  ];



  //------------------------------------------------
	//                RELATIONSHIPS
	//------------------------------------------------

	/**
	* Get all of the answer's
	*
	* @return \Illuminate\Database\Eloquent\Relations\HasMany
	*/
	public function answers()
	{
		return $this->hasMany('App\Models\Answer');
	}
}
