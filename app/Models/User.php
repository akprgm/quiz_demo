<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'name', 'email', 'avatar', 'oauth_type', 'oauth_id', 'score' 
  ];


	//------------------------------------------------
	//                RELATIONSHIPS
	//------------------------------------------------

	/**
	* Get all of the answer's
	*
	* @return \Illuminate\Database\Eloquent\Relations\HasMany
	*/
	public function questionAttempted()
	{
		return $this->hasMany('App\Models\QuestionResponse');
	}
}
