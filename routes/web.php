<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\UserController@index')->name('home');

Route::get('/login', 'Web\UserController@login')->name('login');

Route::get('/oauth/{oauth}', 'Web\UserController@redirectToOauth');

Route::get('/oauth/{oauth}/callback', 'Web\UserController@handleOauthCallback');

Route::group(['middleware' => ['auth']], function(){

	Route::get('/quiz/start', 'Web\QuestionController@index')->name('quiz.start');

	Route::get('/quiz/question/', 'Web\QuestionController@getQuestion')->name('quiz.question');
	
	Route::post('/quiz/question', 'Web\QuestionController@answerQuestion')->name('quiz.answer');

	Route::post('/quiz/start_time', 'Web\QuestionController@storeStartTime');

	Route::get('/end-quiz', 'Web\QuestionController@endQuiz')->name('quiz.end');

	Route::get('/leader-board', 'Web\LeaderBoardController@index')->name('leader-board');

});