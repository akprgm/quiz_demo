<?php

return [
	[ 
		'question'=>'	In the first 10 overs of a cricket game, the run rate was only 3.2. What should be the run rate in the remaining 40 overs to reach the target of 282 runs?',
		'answers' => [
			['6.25', true],
			['6.5', false],
			['6.75', false],
			['7', false],
		]
	],
	[ 
		'question'=>'		A grocer has a sale of Rs. 6435, Rs. 6927, Rs. 6855, Rs. 7230 and Rs. 6562 for 5 consecutive months. How much sale must he have in the sixth month so that he gets an average sale of Rs. 6500?',
		'answers' => [
			['Rs. 4991', true],
			['Rs. 5991', false],
			['Rs. 6001', false],
			['Rs. 6991', false],
		]
	],
	[ 
		'question'=>'	The average of 20 numbers is zero. Of them, at the most, how many may be greater than zero??',
		'answers' => [
			['0', false],
			['1', false],
			['10', false],
			['19', true],
		]
	],
	[ 
		'question'=>'The average weight of 8 persons increases by 2.5 kg when a new person comes in place of one of them weighing 65 kg. What might be the weight of the new person??',
		'answers' => [
			['76 kg', false],
			['76.5 kg', false],
			['Data inadequate', true],
			['None of these', false],
		]
	],
	[ 
		'question'=>'The captain of a cricket team of 11 members is 26 years old and the wicket keeper is 3 years older. If the ages of these two are excluded, the average age of the remaining players is one year less than the average age of the whole team. What is the average age of the team??',
		'answers' => [
			['23 years', true],
			['24 years', false],
			['25 years', false],
			['None of these', false],
		]
	],
	[ 
		'question'=>'	The average monthly income of P and Q is Rs. 5050. The average monthly income of Q and R is Rs. 6250 and the average monthly income of P and R is Rs. 5200. The monthly income of P is:?',
		'answers' => [
			['3500', false],
			['4000', true],
			['4050', false],
			['5000', false],
		]
	],
	[ 
		'question'=>'	The average age of husband, wife and their child 3 years ago was 27 years and that of wife and the child 5 years ago was 20 years. The present age of the husband is:?',
		'answers' => [
			['35 years', false],
			['40 years', true],
			['45 years', false],
			['None of these', false],
		]
	],
	[ 
		'question'=>'	A car owner buys petrol at Rs.7.50, Rs. 8 and Rs. 8.50 per litre for three successive years. What approximately is the average cost per litre of petrol if he spends Rs. 4000 each year??',
		'answers' => [
			['Rs. 7.98', true],
			['Rs. 8', false],
			['Rs. 8.50', false],
			['Rs. 9', false],
		]
	],
	[ 
		'question'=>'In Arun opinion, his weight is greater than 65 kg but less than 72 kg. His brother doest not agree with Arun and he thinks that Arun weight is greater than 60 kg but less than 70 kg. His mother view is that his weight cannot be greater than 68 kg. If all are them are correct in their estimation, what is the average of different probable weights of Arun??',
		'answers' => [
			['67 kg.', true],
			['68 kg.', false],
			['69 kg.', false],
			['None of these', false],
		]
	],
	[ 
		'question'=>'	The average weight of A, B and C is 45 kg. If the average weight of A and B be 40 kg and that of B and C be 43 kg, then the weight of B is:?',
		'answers' => [
			['17 kg', false],
			['20 kg', false],
			['26 kg', false],
			['31 kg', true],
		]
	],
	[ 
		'question'=>'The average weight of 16 boys in a class is 50.25 kg and that of the remaining 8 boys is 45.15 kg. Find the average weights of all the boys in the class.?',
		'answers' => [
			['47.55 kg', false],
			['48 kg', false],
			['48.55 kg', true],
			['49.25 kg', false],
		]
	],
	[ 
		'question'=>'A library has an average of 510 visitors on Sundays and 240 on other days. The average number of visitors per day in a month of 30 days beginning with a Sunday is:?',
		'answers' => [
			['250', false],
			['276', false],
			['280', false],
			['285', true],
		]
	],
	[ 
		'question'=>'A pupil marks were wrongly entered as 83 instead of 63. Due to that the average marks for the class got increased by half (1/2). The number of pupils in the class is:?',
		'answers' => [
			['10', false],
			['20', false],
			['40', true],
			['73', false],
		]
	],
	[ 
		'question'=>'	If the average marks of three batches of 55, 60 and 45 students respectively is 50, 55, 60, then the average marks of all the students is:?',
		'answers' => [
			['53.33', false],
			['54.68', true],
			['55', false],
			['None of these', false],
		]
	]
];

?>