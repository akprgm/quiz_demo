<?php

use App\Models\Question;
use App\Models\Answer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quizs = config('questions');

        foreach ($quizs as $key => $quiz) {

        	$question = Question::create(['question' => $quiz['question']]);

        	foreach($quiz['answers'] as $key => $answer) {
        		$question->answers()->create(['answer' => $answer[0], 'is_correct'=>$answer[1]]);
        	}
        }
    }
}
