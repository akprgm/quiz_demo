$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	//js for question page 
	if($('body').hasClass('question')) {
		
		var start_time = Math.round((new Date()).getTime() / 1000);
		var question_id = $('form input[name="question_id"').val();
		var _token = $('form input[name="_token"').val();
		var url = $('form' ).attr( 'action' );
		
		var count_time = 20;
		setInterval(function(){
			$('.start-time').text(count_time-1);
			$('form input[name="end_time"').val(count_time-1);
			count_time-=1;
		},1000);
		
		$.ajax({
			url: '/quiz/start_time',
			type: 'POST',
			dataType: 'json',
			data: {
				_tokne: _token,
				start_time: start_time, 
				question_id: question_id,
			},
		})
		.done(function() {
			setTimeout(function(){
				$('.question form').submit();
			}, 20000);
		})
		.fail(function() {
			alert("Unable to process, please try later");
		})
	}
});

